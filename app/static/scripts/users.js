function changeSelectValues(e) {
    let x = (!(this.dataset && "id" in this.dataset) ? document.getElementById(user.value) : this); // откуда мы пришли - из селекта или из таблицы
    user.value = x.dataset.id; // устанавливаем значение в select
    [document.getElementById("disable").hidden,
    document.getElementById("enable").hidden,
    document.getElementById("change_password").innerHTML,
    document.getElementById("edit").innerHTML] = document.getElementById("current_user").dataset.id == x.dataset.id ? 
        [true, true, "Сменить пароль", "Личный кабинет"] : 
        (x.dataset.status == "True" ? 
            [false, true] : 
            [true, false]).concat(["Сбросить пароль", "Изменить"]); // пересортим options статуса
    action.value = "edit"; // устанавливаю нейтральный option
    selected.removeAttribute("class"); // убираем класс с прошлого выбранного
    selected = document.getElementById(user.value); // берём текущий выбранный
    selected.setAttribute("class", "selected-row"); // ставим в него класс
}

const user = document.getElementById("user_id"); // селект с user-ами
const action = document.getElementById("action"); // селект с action
[...action.options].forEach(function (e) {e.setAttribute("id", e.value);}); // устанавливаю для каждого option id, их значения (disable, enable, edit)
let selected = document.getElementById(user.value); // выбранная строка в таблице
user.addEventListener("change", changeSelectValues); // вешаем изменение селектов при изменении в форме
const users = [...document.getElementsByTagName("tr")]; // строки с данными пользователей
users.slice(1).forEach(function (e) {e.addEventListener("click", changeSelectValues);}); // вешаем изменение селектов при клике на строку в таблице (нулевой - заголовок таблицы).
changeSelectValues(); // выставляем options действия в соответствии с выбранным изначально