class Base:

    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)

    
    def __eq__(self, __value: object) -> bool:
        return self.id == __value.id

    def __hash__(self) -> int:
        return id(self)

    def __str__(self) -> str:
        return str(self.__dict__)
    
    def __repr__(self) -> str:
        return str(self.__dict__)
