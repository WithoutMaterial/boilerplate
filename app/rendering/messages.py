class Message(dict):
    text: str
    category: str
    title: str

    def __init__(self, text):
        super().__init__(text=text)
        self['text'] = text


class Info(Message):
    
    def __init__(self, text):
        super().__init__(text)
        self['category'] = "info"
        self['title'] = "Информация"


class Warning(Message):

    def __init__(self, text):
        super().__init__(text)
        self['category'] = "warning"
        self['title'] = "Внимание!"


class Error(Message):

    def __init__(self, text):
        super().__init__(text)
        self['category'] = "danger"
        self['title'] = "Ошибка!"


class Success(Message):

    def __init__(self, text):
        super().__init__(text)
        self['category'] = "success"
        self['title'] = "Успешно!"