import pathlib

# import secrets; print(secrets.token_hex())
SECRET_KEY = "b494dd837716457b3589c38b073eb2422f7f15379c882a493cd4c05dd4768d7c"

# строка для подключения к БД
DSN = {
    "dbname": "название бд",
    "user": "postgres",
    "password": "пароль к бд",
    "port": 5432,
    "host": "localhost"
}

# директории
PROJECT_DIR = pathlib.Path(__file__).resolve().parent.parent
BASE_DIR = PROJECT_DIR.joinpath("app")

# настройка капчи
RECAPTCHA_PUBLIC_KEY = "6Lcu638pAAAAAGv2GpVs2JmwpeT0bS7c9dyeDTmv"
RECAPTCHA_PRIVATE_KEY = "6Lcu638pAAAAAM-uHbCItN3TpDezVh9Jjv_5RgSO"

# настройка SMTP
SERVER_EMAIL = "engp.web.support@ez.meteor.ru"
SMTP = {
    'host': "mail.ez.meteor.ru",
    'port': 25
}
