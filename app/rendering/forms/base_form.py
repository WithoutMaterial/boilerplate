from collections import OrderedDict
from flask_wtf import FlaskForm
from wtforms import SubmitField

class BaseForm(FlaskForm):
    submit = SubmitField("Подтвердить")
    
    def __init__(self, *args, **kwargs):
        # TODO: разобрать принцип работы.
        field_order = getattr(self, 'field_order', None)
        if field_order:
            temp_fields = []
            for name in field_order:
                if name == '*':
                    temp_fields.extend([f for f in self._unbound_fields if f[0] not in field_order])
                else:
                    temp_fields.append([f for f in self._unbound_fields if f[0] == name and name != "submit"][0])
                temp_fields.append([f for f in self._unbound_fields if f[0 == "submit"]][0])
            self._unbound_fields = temp_fields
        super().__init__(*args, **kwargs)
        self._fields = OrderedDict((k[0], self._fields[k[0]]) for k in self._unbound_fields)