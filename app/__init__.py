from flask import Flask, render_template


def create_app():
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_pyfile("config.py")

    from app import db

    db.init_app(app)

    @app.route('/')
    def index():
        return render_template("base.html")

    return app
