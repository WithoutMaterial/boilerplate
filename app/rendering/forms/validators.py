import re
from wtforms import ValidationError


def base_validate(field, params):
    """Базовая валидация."""
    for condition, message in params:
        if not condition:
            field.errors.append(message)
    if field.errors:
        raise ValidationError(field.errors.pop())
    

def validate_password(form, field):
    """
    Валидация формата пароля
    - содержит строчные и прописные латинские буквы
    - содержит цифры
    - содержит специальные символы
    - длина пароля >= 12
    """
    # TODO: вынести в users
    password = field.data
    params = [
        (
            re.search("[a-z]", password),
            "Пароль должен содержать строчные латинские буквы!",
        ),
        (
            re.search("[A-Z]", password),
            "Пароль должен содержать прописные латинские буквы!",
        ),
        (re.search("[0-9]", password), "Пароль должен содержать цифры!"),
        (
            re.search("[^a-zA-Z0-9]", password),
            "Пароль должен содержать специальные символы!",
        ),
        (len(password) >= 12, "Пароль должен содержать не менее 12 символов!"),
    ]
    base_validate(field, params)